package com.hexefy.notes

import android.arch.core.executor.testing.InstantTaskExecutorRule
import com.hexefy.notes.model.database.converters.Status
import com.hexefy.notes.model.database.entities.NoteEntity
import com.hexefy.notes.model.database.repositories.notes.NotesRepository
import com.hexefy.notes.viewModel.NotesViewModel
import io.reactivex.Single
import junit.framework.Assert.assertNotNull
import org.hamcrest.MatcherAssert
import org.hamcrest.Matchers
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4
import org.mockito.ArgumentMatchers
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.MockitoAnnotations
import java.util.*

/**
 * Created by Ondrej Synak on 3/23/19.
 *
 * Testing functionality of NotesViewModel
 */
@RunWith(JUnit4::class)
class NoteViewModelTest {

	@get:Rule
	val instantTaskExecutorRule = InstantTaskExecutorRule()

	@get:Rule
	val rxSchedulerRule = RxSchedulerRule()

	@Mock
	lateinit var notesRepository: NotesRepository

	private lateinit var viewModel: NotesViewModel

	private val testData =
			Arrays.asList(
					NoteEntity(1, 1, "One", Status.UP_TO_DATE),
					NoteEntity(2, 2, "Two", Status.UP_TO_DATE)
			)

	@Before
	fun before() {
		MockitoAnnotations.initMocks(this)

		mockNotesRepositoryResponses()
		viewModel = NotesViewModel(notesRepository)
	}

	@Test
	fun testRefreshData() {
		viewModel.refreshData()

		assertNotNull(viewModel.notes.value)
		MatcherAssert.assertThat(viewModel.loadingProgressBarVisibility.value, Matchers.`is`(false))
	}

	@Test
	fun testDatabaseRefreshData() {
		viewModel.refreshData(false)

		assertNotNull(viewModel.notes.value)
		MatcherAssert.assertThat(viewModel.loadingProgressBarVisibility.value, Matchers.`is`(false))
	}


	@Test
	fun testCreateNewNote() {
		val title = "Hi"
		viewModel.saveNote(null, title)

		Mockito.verify(notesRepository).createNote(title)
	}

	@Test
	fun testUpdateNote() {
		val noteEntity = NoteEntity(5, null, "Hello", Status.CREATED)

		viewModel.saveNote(noteEntity.id, noteEntity.title)

		Mockito.verify(notesRepository).updateNote(noteEntity.id!!, noteEntity.title)
	}

	@Test
	fun testGetNode() {
		val exceptedNoteEntity = testData[0]
		val noteEntity = viewModel.getNote(exceptedNoteEntity.id!!)

		assertNotNull(noteEntity)
		MatcherAssert.assertThat(noteEntity, Matchers.`is`(exceptedNoteEntity))
	}

	/**
	 * Mock repository responses.
	 */
	private fun mockNotesRepositoryResponses() {
		Mockito.`when`(notesRepository.listAllNotes(ArgumentMatchers.anyBoolean()))
				.thenAnswer {
					return@thenAnswer Single.just(testData)
				}

		Mockito.`when`(notesRepository.createNote(ArgumentMatchers.anyString()))
				.thenAnswer {
					return@thenAnswer Single.just(NoteEntity(5, null, "Hello", Status.CREATED))
				}

		Mockito.`when`(notesRepository.updateNote(ArgumentMatchers.anyInt(), ArgumentMatchers.anyString()))
				.thenAnswer {
					return@thenAnswer Single.just(NoteEntity(5, 4, "How are you", Status.UPDATED))
				}
	}
}