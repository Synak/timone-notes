package com.hexefy.notes.utilitties

import android.graphics.Canvas
import android.graphics.Color
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.helper.ItemTouchHelper
import com.hexefy.notes.view.overview.adapter.NotesAdapter
import android.graphics.drawable.ColorDrawable
import android.graphics.drawable.Drawable
import android.support.v4.content.ContextCompat
import com.hexefy.notes.R

/**
 * Created by Ondrej Synak on 3/23/19.
 *
 * Managing swipe to delete functionality.
 */
class SwipeToDeleteCallback : ItemTouchHelper.SimpleCallback {

	private val adapter: NotesAdapter
	private val icon: Drawable
	private val background: ColorDrawable

	constructor(adapter: NotesAdapter) : super(0, ItemTouchHelper.LEFT or ItemTouchHelper.RIGHT) {
		this.adapter = adapter
		icon = ContextCompat.getDrawable(adapter.context, R.drawable.baseline_delete_24)!!
		background = ColorDrawable(Color.RED)
	}

	override fun onMove(p0: RecyclerView, p1: RecyclerView.ViewHolder, p2: RecyclerView.ViewHolder) = false

	override fun onSwiped(viewHolder: RecyclerView.ViewHolder, direction: Int) {
		val position = viewHolder.adapterPosition
		adapter.deleteItem(position)
	}

	override fun onChildDraw(canvas: Canvas, recyclerView: RecyclerView, viewHolder: RecyclerView.ViewHolder, dX: Float, dY: Float, actionState: Int, isCurrentlyActive: Boolean) {
		super.onChildDraw(canvas, recyclerView, viewHolder, dX,
				dY, actionState, isCurrentlyActive)

		val itemView = viewHolder.itemView
		val backgroundCornerOffset = 20

		val iconMargin = (itemView.height - icon.intrinsicHeight) / 2
		val iconTop = itemView.top + (itemView.height - icon.intrinsicHeight) / 2
		val iconBottom = iconTop + icon.intrinsicHeight

		when {
			dX > 0 -> { // Swiping to the right
				val iconLeft = itemView.left + iconMargin + icon.intrinsicWidth
				val iconRight = itemView.left + iconMargin
				icon.setBounds(iconLeft, iconTop, iconRight, iconBottom)

				background.setBounds(itemView.left, itemView.top,
						itemView.left + dX.toInt() + backgroundCornerOffset,
						itemView.bottom)
			}
			dX < 0 -> { // Swiping to the left
				val iconLeft = itemView.right - iconMargin - icon.intrinsicWidth
				val iconRight = itemView.right - iconMargin
				icon.setBounds(iconLeft, iconTop, iconRight, iconBottom)

				background.setBounds(itemView.right + dX.toInt() - backgroundCornerOffset,
						itemView.top, itemView.right, itemView.bottom)
			}
			else -> // view is unSwiped
				background.setBounds(0, 0, 0, 0)
		}

		background.draw(canvas)
		icon.draw(canvas)
	}
}