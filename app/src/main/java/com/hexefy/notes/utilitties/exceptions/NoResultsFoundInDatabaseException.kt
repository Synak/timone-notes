package com.hexefy.notes.utilitties.exceptions

/**
 * Created by Ondrej Synak on 3/23/19.
 *
 * Exception should be user after repository tried to load online data but failed and local storage is also
 * empty.
 */
class NoResultsFoundInDatabaseException(message: String?) : RuntimeException(message)