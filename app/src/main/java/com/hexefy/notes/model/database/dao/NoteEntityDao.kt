package com.hexefy.notes.model.database.dao

import android.arch.persistence.room.Dao
import android.arch.persistence.room.Insert
import android.arch.persistence.room.OnConflictStrategy
import android.arch.persistence.room.Query
import com.hexefy.notes.model.database.converters.Status
import com.hexefy.notes.model.database.entities.NoteEntity
import java.util.*

/**
 * Created by Ondrej Synak on 3/22/19.
 */
@Dao
interface NoteEntityDao {

	@Insert(onConflict = OnConflictStrategy.REPLACE)
	fun save(note: NoteEntity)

	@Query("DELETE FROM noteEntity WHERE id = :id")
	fun delete(id: Int)

	@Query("DELETE FROM noteEntity WHERE serverId = :serverId")
	fun deleteByServerId(serverId: Int)

	@Insert(onConflict = OnConflictStrategy.REPLACE)
	fun saveAll(notes: List<NoteEntity>): LongArray

	@Query("SELECT * FROM noteEntity WHERE status != :status")
	fun getAllToDisplay(status: Status = Status.DELETED): List<NoteEntity>

	@Query("DELETE FROM noteEntity WHERE status == :status")
	fun deleteAllByStatus(status: Status)

	@Query("SELECT * FROM noteEntity WHERE status IN(:statuses) ")
	fun getAllForUpdate(
			statuses: List<Int> = Arrays.asList(
					Status.CREATED.code,
					Status.DELETED.code,
					Status.UPDATED.code
			)
	): List<NoteEntity>

	@Query("SELECT * FROM noteEntity")
	fun getAll(): List<NoteEntity>

	@Query("SELECT * FROM noteEntity WHERE id = :noteId")
	fun get(noteId: Int): NoteEntity

	@Query("SELECT * FROM noteEntity WHERE serverId = :serverId")
	fun getByServerId(serverId: Int): NoteEntity

	@Query("SELECT COUNT(*) FROM noteEntity")
	fun count(): Long
}