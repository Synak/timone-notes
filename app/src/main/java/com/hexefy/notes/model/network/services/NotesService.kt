package com.hexefy.notes.model.network.services

import com.hexefy.notes.model.network.dataModels.NoteModel
import io.reactivex.Single
import retrofit2.Response
import retrofit2.http.*

/**
 * Created by Ondrej Synak on 3/22/19.
 */

interface NotesService {

	@GET("notes")
	fun listAllNotes(): Single<Response<List<NoteModel>>>

	@POST("notes")
	fun createNote(@Body note: NoteModel): Single<Response<NoteModel>>

	@GET("notes/{id}")
	fun retriveNote(@Path("id") noteId: Int): Single<Response<NoteModel>>

	@PUT("notes/{id}")
	fun updateNote(@Path("id") noteId: Int, @Body note: NoteModel): Single<Response<NoteModel>>

	@DELETE("notes/{id}")
	fun deleteNote(@Path("id") noteId: Int): Single<Response<NoteModel>>
}