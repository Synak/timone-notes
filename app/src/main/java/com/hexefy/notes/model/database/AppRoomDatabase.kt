package com.hexefy.notes.model.database

import android.arch.persistence.room.Database
import android.arch.persistence.room.RoomDatabase
import android.arch.persistence.room.TypeConverters
import com.hexefy.notes.model.database.converters.StatusConverter
import com.hexefy.notes.model.database.dao.NoteEntityDao
import com.hexefy.notes.model.database.entities.NoteEntity

/**
 * Created by Ondrej Synak on 3/22/19.
 */
@Database(
		entities = [NoteEntity::class],
		version = 1, exportSchema = false
)
@TypeConverters(StatusConverter::class)
abstract class AppRoomDatabase : RoomDatabase() {

	abstract fun provideNoteEntityDao(): NoteEntityDao
}
