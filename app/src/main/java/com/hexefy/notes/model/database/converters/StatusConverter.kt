package com.hexefy.notes.model.database.converters

import android.arch.persistence.room.TypeConverter
import java.lang.IllegalArgumentException

/**
 * Created by Ondrej Synak on 3/22/19.
 * Converts Int to Status and back, needed for Room database.
 */
class StatusConverter {

	@TypeConverter
	fun fromInt(value: Int): Status {
		return when (value) {
			Status.UP_TO_DATE.code -> Status.UP_TO_DATE
			Status.DELETED.code -> Status.DELETED
			Status.UPDATED.code -> Status.UPDATED
			Status.CREATED.code -> Status.CREATED
			else -> throw IllegalArgumentException("Int cannot be converted to Status!")
		}
	}

	@TypeConverter
	fun statusToInt(status: Status): Int? {
		return status.code
	}
}

/**
 * Represents Status of NoteEntity for synchronizing local database with server.
 */
enum class Status(val code: Int) {
	UP_TO_DATE(0),
	DELETED(1),
	UPDATED(2),
	CREATED(3)
}