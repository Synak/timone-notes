package com.hexefy.notes.model.network

import com.hexefy.notes.model.network.dataModels.NoteModel
import com.hexefy.notes.model.network.services.NotesService
import io.reactivex.Single
import retrofit2.Response
import retrofit2.Retrofit
import java.lang.IllegalArgumentException

/**
 * Created by Ondrej Synak on 3/22/19.
 * Helper for calling RestAPI via Retrofit.
 */
class RestHelper(retrofit: Retrofit) {

	// Services ************************************************************************************

	private val notesService: NotesService = retrofit.create(NotesService::class.java)

	// Public Methods ******************************************************************************

	fun listAllNotes() = notesService.listAllNotes()

	fun createNote(note: NoteModel) = notesService.createNote(note)

	fun retrieveNote(noteId: Int) = notesService.retriveNote(noteId)

	fun updateNote(note: NoteModel): Single<Response<NoteModel>> {
		if (note.id == null) {
			throw IllegalArgumentException("note.id cannot be null")
		}

		return notesService.updateNote(note.id, note)
	}

	fun deleteNote(noteId: Int) = notesService.deleteNote(noteId)
}