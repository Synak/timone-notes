package com.hexefy.notes.model.database.repositories.notes

import com.hexefy.notes.model.database.AppRoomDatabase
import com.hexefy.notes.model.database.converters.Status
import com.hexefy.notes.model.database.entities.NoteEntity
import com.hexefy.notes.model.network.RestHelper
import com.hexefy.notes.model.network.dataModels.NoteModel
import com.hexefy.notes.utilitties.exceptions.NoResultsFoundInDatabaseException
import io.reactivex.Single
import io.reactivex.schedulers.Schedulers
import retrofit2.Response
import timber.log.Timber
import javax.inject.Inject
import javax.inject.Singleton

/**
 * Created by Ondrej Synak on 3/22/19.
 *
 * Repository for managing loading, saving, updating and deleting notes
 */
@Singleton
class NotesRepository @Inject constructor(
		private val restHelper: RestHelper,
		appRoomDatabase: AppRoomDatabase
) {

	// Private Attributes **************************************************************************

	private val noteEntityDao = appRoomDatabase.provideNoteEntityDao()

	// Public Methods ******************************************************************************

	/**
	 * Fetch note entities from server and save it to database, if @param fromDatabase is true than just fetch data
	 * from database, if server return error, than return entities saved in database.
	 */
	fun listAllNotes(fromDatabase: Boolean = false): Single<List<NoteEntity>> {
		Timber.v("listAllNotes() fromDatabase: $fromDatabase")

		if (!fromDatabase) {
			return synchWithServer()
					.flatMap { restHelper.listAllNotes() }
					.map { response ->
						if (response.isSuccessful) {
							response.body()?.let { notes ->
								noteEntityDao.deleteAllByStatus(Status.UP_TO_DATE)

								noteEntityDao.saveAll(notes.mapNotNull {
									it.toEntity()
								})
							}
						} else {
							// TODO define exception according to RESTAPI.
							throw Exception(response.errorBody().toString())
						}
						noteEntityDao.getAllToDisplay()
					}
					.onErrorResumeNext {
						if (noteEntityDao.count() == 0L) {
							throw NoResultsFoundInDatabaseException("Cache is empty.")
						} else {
							Single.just(noteEntityDao.getAllToDisplay())
						}
					}
		} else {
			return Single.just(noteEntityDao.getAllToDisplay())
		}
	}

	/**
	 * Create new Note Entity and save it locally and also try to save it on server, if it fails then set entity
	 * status to CREATED and save it to database.
	 */
	fun createNote(title: String): Single<NoteEntity> {
		Timber.v("createNote() title: $title")

		val entity = NoteEntity(null, null, title, Status.CREATED)

		return createdOrUpdatedNote(restHelper.createNote(entity.toModel()), entity)
	}

	/**
	 * Update Note Entity and save it locally and also try to update it on server, if it fails then set entity
	 * status to UPDATED and save it to database.
	 */
	fun updateNote(noteId: Int, title: String): Single<NoteEntity> {
		Timber.v("updateNote() noteId: $noteId, title: $title")

		val noteEntity = noteEntityDao.get(noteId)
		val request: Single<Response<NoteModel>>

		noteEntity.title = title

		// Check if note was created only locally, if yes then try to createNode.
		if (noteEntity.status != Status.CREATED) {
			noteEntity.status = Status.UPDATED
			request = restHelper.updateNote(noteEntity.toModel())
		} else {
			request = restHelper.createNote(noteEntity.toModel())
		}

		return createdOrUpdatedNote(request, noteEntity)
	}

	/**
	 * Delete Note Entity locally and also try to delete it on server,  if it fails then set entity
	 * status to DELETED and save it to database.
	 */
	fun deleteNote(noteId: Int): Single<Int> {
		Timber.v("deleteNote() noteId: $noteId")

		val noteEntity = noteEntityDao.get(noteId)

		// If the note was never uploaded to server, delete it just locally.
		if (noteEntity.status == Status.CREATED) {
			noteEntityDao.delete(noteId)

			return Single.just(noteId)
		} else {
			noteEntity.status = Status.DELETED

			return restHelper.deleteNote(noteId)
					.map { response ->
						if (response.isSuccessful) {
							noteEntityDao.delete(noteId)
						} else {
							// TODO define exception according to RESTAPI.
							throw Exception(response.errorBody().toString())
						}

						noteEntity.serverId!!
					}
					.onErrorResumeNext {
						noteEntityDao.save(noteEntity)
						Single.just(noteEntity.serverId)
					}
		}
	}


	fun retrieveNote(noteId: Int) {

	}

	// Private Methods *****************************************************************************

	/**
	 * Synchronize note entities which needs update with server,
	 * that means entities with status CREATED, DELETED or UPDATED.
	 */
	private fun synchWithServer(): Single<Unit> {
		Timber.v("synchWithServer()")

		return Single.fromCallable {
			noteEntityDao.getAllForUpdate().forEach {
				when (it.status) {
					Status.UPDATED -> updateNote(it.serverId!!, it.title).blockingGet()
					Status.DELETED -> deleteNote(it.id!!).blockingGet()
					Status.CREATED -> createNote(it).blockingGet()
					else -> Timber.v("Skipping up to date note.")
				}
			}
		}.subscribeOn(Schedulers.computation())
	}

	/**
	 * Create note from entity, used only for locally created new entities and not created on server.
	 */
	private fun createNote(noteEntity: NoteEntity): Single<NoteEntity> {
		Timber.v("createNote() note: $noteEntity")

		return createdOrUpdatedNote(restHelper.createNote(noteEntity.toModel()), noteEntity)
	}

	/**
	 * After updating or creating note entity, set correct status and server id, or if it fails
	 * then save it locally with status Created or Updated.
	 */
	private fun createdOrUpdatedNote(
			request: Single<Response<NoteModel>>,
			noteEntity: NoteEntity
	): Single<NoteEntity> {
		return request.map { response ->
			if (response.isSuccessful) {
				response.body()?.let { noteModel ->
					with(noteEntity) {
						serverId = noteModel.id
						status = Status.UP_TO_DATE
						title = noteModel.title
						noteEntityDao.save(this)
					}
				}
			} else {
				// TODO define exception according to RESTAPI.
				throw Exception()
			}

			noteEntity
		}
				.onErrorResumeNext {
					noteEntityDao.save(noteEntity)
					Single.just(noteEntity)
				}
	}
}
