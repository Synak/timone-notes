package com.hexefy.notes.model.database.entities

import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey
import com.hexefy.notes.model.database.converters.Status
import com.hexefy.notes.model.network.dataModels.NoteModel

/**
 * Created by Ondrej Synak on 3/22/19.
 */
@Entity(tableName = "noteEntity")
data class NoteEntity(
		@PrimaryKey(autoGenerate = true) var id: Int?,
		var serverId: Int? = null,
		var title: String,
		var status: Status
) {
	fun toModel(): NoteModel {
		return NoteModel(serverId, title)
	}
}