package com.hexefy.notes.model.network.dataModels

import com.hexefy.notes.model.database.converters.Status
import com.hexefy.notes.model.database.entities.NoteEntity

/**
 * Created by Ondrej Synak on 3/22/19.
 */
data class NoteModel(val id: Int? = null, val title: String) {
	fun toEntity(): NoteEntity? {
		return if (id != null) {
			NoteEntity(null, id, title, Status.UP_TO_DATE)
		} else {
			null
		}
	}
}