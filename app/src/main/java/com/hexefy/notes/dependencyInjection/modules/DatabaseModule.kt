package com.hexefy.notes.dependencyInjection.modules

import android.arch.persistence.room.Room
import android.content.Context
import com.hexefy.notes.model.database.AppRoomDatabase
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

/**
 * Created by Ondrej Synak on 3/22/19.
 */
@Module
class DatabaseModule {

	@Provides
	@Singleton
	fun provideDatabase(context: Context): AppRoomDatabase =
			Room.databaseBuilder(context, AppRoomDatabase::class.java, "note_database")
					.allowMainThreadQueries()
					.build()
}