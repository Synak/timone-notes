package com.hexefy.notes.dependencyInjection.modules

import com.google.gson.Gson
import com.hexefy.notes.model.network.RestHelper
import dagger.Module
import dagger.Provides
import io.reactivex.schedulers.Schedulers
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import timber.log.Timber
import javax.inject.Singleton

/**
 * Created by Ondrej Synak on 3/22/19.
 */
@Module
class NetworkModule(private val restApiUrl: String) {

	@Provides
	@Singleton
	fun provideOkHttpClient(): OkHttpClient = OkHttpClient.Builder()
			.addInterceptor(HttpLoggingInterceptor { message -> Timber.v(message) }.setLevel(HttpLoggingInterceptor.Level.BODY))
			.build()

	@Provides
	@Singleton
	fun provideRetrofit(okHttpClient: OkHttpClient): Retrofit = Retrofit.Builder()
			.baseUrl(restApiUrl)
			.client(okHttpClient)
			.addConverterFactory(GsonConverterFactory.create())
			.addCallAdapterFactory(RxJava2CallAdapterFactory.createWithScheduler(Schedulers.io()))
			.build()

	@Provides
	@Singleton
	fun provideRestHelper(retrofit: Retrofit) = RestHelper(retrofit)

	@Provides
	@Singleton
	fun provideGson() = Gson()
}