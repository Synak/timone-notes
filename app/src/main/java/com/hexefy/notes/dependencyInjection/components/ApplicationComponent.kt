package com.hexefy.notes.dependencyInjection.components

import com.hexefy.notes.dependencyInjection.modules.AppModule
import com.hexefy.notes.dependencyInjection.modules.DatabaseModule
import com.hexefy.notes.dependencyInjection.modules.NetworkModule
import com.hexefy.notes.dependencyInjection.modules.viewModel.ViewModelModule
import com.hexefy.notes.view.base.BaseNotesFragment
import com.hexefy.notes.viewModel.NotesViewModel
import dagger.Component
import javax.inject.Singleton

/**
 * Created by Ondrej Synak on 3/22/19.
 */
@Singleton
@Component(modules = [AppModule::class, NetworkModule::class, DatabaseModule::class, ViewModelModule::class])
interface ApplicationComponent {
	fun inject(baseNotesFragment: BaseNotesFragment)
}