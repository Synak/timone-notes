package com.hexefy.notes.dependencyInjection.modules

import android.arch.lifecycle.ViewModel
import android.arch.lifecycle.ViewModelProvider
import android.content.Context
import com.hexefy.notes.App
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

/**
 * Created by Ondrej Synak on 3/22/19.
 */
@Module
class AppModule(private val context: Context) {

	@Provides
	fun provideContext() = context

}