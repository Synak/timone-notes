package com.hexefy.notes.viewModel

import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import com.hexefy.notes.model.database.entities.NoteEntity
import com.hexefy.notes.model.database.repositories.notes.NotesRepository
import com.hexefy.notes.utilitties.SingleLiveEvent
import com.hexefy.notes.utilitties.exceptions.NoResultsFoundInDatabaseException
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.functions.Consumer
import timber.log.Timber
import javax.inject.Inject

/**
 * Created by Ondrej Synak on 3/22/19.
 *
 * ViewModel for note/s oprerations.
 */
class NotesViewModel : ViewModel {

	// Private Attributes **************************************************************************

	private val compositeDisposable: CompositeDisposable

	// Dependency Injection ************************************************************************

	private var notesRepository: NotesRepository

	// Live Data ***********************************************************************************

	/**
	 * LiveData with list of current notes.
	 */
	val notes: MutableLiveData<List<NoteEntity>> = MutableLiveData()
	/**
	 * LiveData with flag if loading progress bar should be visible.
	 */
	val loadingProgressBarVisibility: MutableLiveData<Boolean> = MutableLiveData()
	/**
	 * Gets called when either update or create operation is finished.
	 */
	val updateCreateOrDeleteOperationFinished: SingleLiveEvent<Void> = SingleLiveEvent()

	// Public Methods ******************************************************************************

	@Inject
	constructor(notesRepository: NotesRepository) : super() {
		this.notesRepository = notesRepository
		compositeDisposable = CompositeDisposable()
		refreshData()
	}


	/**
	 * Load all notes
	 *
	 * @param fromDatabase True if data should be loaded only from database and not from net.
	 */
	fun refreshData(fromDatabase: Boolean = false) {
		Timber.v("refreshData()")

		compositeDisposable.add(
				notesRepository
						.listAllNotes(fromDatabase)
						.observeOn(AndroidSchedulers.mainThread())
						.doOnSubscribe {
							if (!fromDatabase) {
								loadingProgressBarVisibility.postValue(true)
							}
						}
						.doAfterTerminate {
							if (!fromDatabase) {
								loadingProgressBarVisibility.postValue(false)
							}
						}
						.subscribe({
							notes.postValue(it)
						}, {
							Timber.e(it)
							if (it is NoResultsFoundInDatabaseException) {
								Timber.d("Something went wrong with online fetching and local storage is also empty.")
							}
						})
		)
	}

	/**
	 * Create note and save it
	 *
	 * @param title note text.
	 */
	fun saveNote(noteId: Int? = null, title: String) {
		Timber.v("saveNote() title: $title")
		val request = if (noteId == null) {
			notesRepository.createNote(title)
		} else {
			notesRepository.updateNote(noteId, title)
		}

		compositeDisposable.add(
				request
						.doOnSubscribe { loadingProgressBarVisibility.postValue(true) }
						.doAfterTerminate { loadingProgressBarVisibility.postValue(false) }
						.observeOn(AndroidSchedulers.mainThread())
						.subscribe(getSaveOrDeleteSuccessConsumer(true), getSaveOrDeleteErrorConsumer())
		)
	}

	/**
	 * Delete Note with @param noteId id
	 */
	fun deleteNote(noteId: Int, inDetail: Boolean = false) {
		Timber.v("deleteNote() id: $noteId")

		compositeDisposable.add(
				notesRepository.deleteNote(noteId)
						.doOnSubscribe {
							if (inDetail) {
								loadingProgressBarVisibility.postValue(true)
							}
						}
						.doAfterTerminate { loadingProgressBarVisibility.postValue(false) }
						.observeOn(AndroidSchedulers.mainThread())
						.subscribe(getSaveOrDeleteSuccessConsumer(inDetail), getSaveOrDeleteErrorConsumer())
		)
	}

	/**
	 * Return NoteEntity if there is any with @param noteId
	 */
	fun getNote(noteId: Int): NoteEntity? = notes.value?.find { noteEntity -> noteEntity.id == noteId }

	override fun onCleared() {
		super.onCleared()
		Timber.v("onCleared()")

		compositeDisposable.dispose()
	}

	// Private Methods *****************************************************************************

	/**
	 * Creates Consumer for handling error state for deletion and save of note
	 */
	private fun <T : Throwable> getSaveOrDeleteErrorConsumer(): Consumer<T> {
		return Consumer {
			Timber.e(it)
			updateCreateOrDeleteOperationFinished.call()
		}
	}

	/**
	 * Creates Consumer for handling success state for deletion and save of note
	 */
	private fun <T> getSaveOrDeleteSuccessConsumer(callback: Boolean): Consumer<T> {
		return Consumer {
			if (callback) {
				updateCreateOrDeleteOperationFinished.call()
			}
			refreshData(true)
		}
	}
}