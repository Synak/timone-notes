package com.hexefy.notes.view.detail

import android.arch.lifecycle.Observer
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.*
import com.hexefy.notes.R
import com.hexefy.notes.view.base.BaseNotesFragment
import kotlinx.android.synthetic.main.fragment_note_detail.*
import timber.log.Timber

/**
 * Created by Ondrej Synak on 3/22/19.
 *
 * Fragment for showing detail of note entity to edit it or to create a new one.
 */
class NoteDetailFragment : BaseNotesFragment() {

	companion object {
		private const val NOTE_ID = "NOTE_ID"

		/**
		 * Create new instance of this fragment with optional argument - note id, if note id is set,
		 * update note if not then create one.
		 */
		fun newInstance(noteId: Int? = null): NoteDetailFragment {
			val fragment = NoteDetailFragment()

			noteId?.let { id ->
				val bundle = Bundle().also {
					it.putInt(NOTE_ID, id)
				}
				fragment.arguments = bundle
			}

			return fragment
		}
	}

	// Private Attributes **********************************************************************************************

	private var noteId: Int? = null

	// Public Methods **************************************************************************************************

	override fun onCreate(savedInstanceState: Bundle?) {
		super.onCreate(savedInstanceState)
		setHasOptionsMenu(true)
	}

	override fun onPrepareOptionsMenu(menu: Menu) {
		menu.findItem(R.id.action_delete).isVisible = noteId != null
		super.onPrepareOptionsMenu(menu)
	}

	override fun onOptionsItemSelected(item: MenuItem?): Boolean {
		if (item != null && noteId != null) {
			when (item.itemId) {
				R.id.action_delete -> {
					notesViewModel.deleteNote(noteId!!, true)
				}
			}
		}

		return false
	}

	override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
		noteId = arguments?.get(NOTE_ID) as Int?

		return inflater.inflate(R.layout.fragment_note_detail, container, false)
	}

	override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
		super.onViewCreated(view, savedInstanceState)
		initializeViews()
		observeLiveData()
	}

	// Private Methods *************************************************************************************************

	private fun observeLiveData() {
		Timber.v("observeLiveData()")

		with(notesViewModel) {
			updateCreateOrDeleteOperationFinished.observe(viewLifecycleOwner, Observer {
				activity?.onBackPressed()
			})
			loadingProgressBarVisibility.observe(viewLifecycleOwner, Observer { value ->
				value?.let {
					progress_bar_container.visibility = if (it) {
						View.VISIBLE
					} else {
						View.GONE
					}
				}
			})
		}
	}

	private fun initializeViews() {
		Timber.v("initializeViews()")

		val actionBar = (activity as AppCompatActivity).supportActionBar

		actionBar?.setDisplayHomeAsUpEnabled(true)
		actionBar?.setDisplayShowHomeEnabled(true)

		if (noteId != null) {
			actionBar?.title = getString(R.string.note_detail_edit_title)
			notesViewModel.getNote(noteId!!)?.let { entity ->
				note_edit_text_view.setText(entity.title)
				note_edit_text_view.setSelection(note_edit_text_view.text.length)
			}
		} else {
			actionBar?.title = getString(R.string.note_detail_create_title)

		}

		save_button.setOnClickListener {
			notesViewModel.saveNote(noteId, note_edit_text_view.text.toString())
		}
	}
}