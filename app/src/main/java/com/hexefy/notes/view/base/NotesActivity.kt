package com.hexefy.notes.view.base

import android.app.Activity
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.Menu
import android.view.View
import android.view.inputmethod.InputMethodManager
import com.hexefy.notes.R
import com.hexefy.notes.view.base.navigation.NavigationController
import kotlinx.android.synthetic.main.activity_main.*

/**
 * Created by Ondrej Synak on 3/22/19.
 *
 * Main activity which is actually here only for managing navigation
 */
class NotesActivity : AppCompatActivity() {

	// Public Attributes ***************************************************************************

	val navigationController: NavigationController =
			NavigationController(supportFragmentManager, R.id.container)

	// Public Methods ******************************************************************************

	override fun onCreate(savedInstanceState: Bundle?) {
		super.onCreate(savedInstanceState)

		setContentView(R.layout.activity_main)

		navigationController.goToOverView()
	}

	override fun onCreateOptionsMenu(menu: Menu?): Boolean {
		menuInflater.inflate(R.menu.menu, menu)
		return true
	}

	override fun onPostCreate(savedInstanceState: Bundle?) {
		super.onPostCreate(savedInstanceState)
		setSupportActionBar(toolbar)
	}

	override fun onSupportNavigateUp(): Boolean {
		onBackPressed()
		return true
	}

	override fun onBackPressed() {
		hideKeyboard(if (currentFocus == null) View(this) else currentFocus)
		super.onBackPressed()
	}

	// Private Methods *****************************************************************************

	private fun hideKeyboard(view: View) {
		val inputMethodManager = getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
		inputMethodManager.hideSoftInputFromWindow(view.windowToken, 0)
	}
}