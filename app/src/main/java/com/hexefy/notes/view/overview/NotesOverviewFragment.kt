package com.hexefy.notes.view.overview

import android.arch.lifecycle.Observer
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.helper.ItemTouchHelper
import android.view.LayoutInflater
import android.view.Menu
import android.view.View
import android.view.ViewGroup
import com.hexefy.notes.R
import com.hexefy.notes.model.database.entities.NoteEntity
import com.hexefy.notes.utilitties.SwipeToDeleteCallback
import com.hexefy.notes.view.base.BaseNotesFragment
import com.hexefy.notes.view.overview.adapter.NotesAdapter
import kotlinx.android.synthetic.main.fragment_notes_overview.*
import timber.log.Timber


/**
 * Created by Ondrej Synak on 3/22/19.
 *
 * Fragment for displaying all note entities present on the device.
 */
class NotesOverviewFragment : BaseNotesFragment(), NotesAdapter.NotesAdapterListener {

	// Private Attributes **************************************************************************

	/**
	 * Adapter for displaying Note Entities
	 */
	private var notesAdapter: NotesAdapter? = null

	// Public Methods ******************************************************************************

	override fun onCreate(savedInstanceState: Bundle?) {
		super.onCreate(savedInstanceState)
		setHasOptionsMenu(true)
	}

	override fun onPrepareOptionsMenu(menu: Menu) {
		menu.findItem(R.id.action_delete).isVisible = false
		super.onPrepareOptionsMenu(menu)
	}

	override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
		return inflater.inflate(R.layout.fragment_notes_overview, container, false)
	}

	override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
		super.onViewCreated(view, savedInstanceState)
		initializeViews()
		observeLiveData()
	}

	override fun onNoteClicked(noteId: Int) {
		navigationController?.goToDetail(noteId)
	}

	override fun onNoteDeleted(noteEntity: NoteEntity) {
		notesViewModel.deleteNote(noteEntity.id!!, false)
	}

	// Private Methods *************************************************************************************************

	private fun observeLiveData() {
		Timber.v("observeLiveData()")

		with(notesViewModel) {
			notes.observe(viewLifecycleOwner, Observer { value ->
				value?.let { items ->
					notesAdapter?.setData(items)
				}
			})
			loadingProgressBarVisibility.observe(viewLifecycleOwner, Observer { value ->
				value?.let {
					swipe_to_refresh.isRefreshing = it
				}
			})
		}
	}

	private fun initializeViews() {
		Timber.v("initializeViews()")

		(activity as AppCompatActivity).supportActionBar?.let {
			it.title = getString(R.string.notes_overview_title)
			it.setDisplayHomeAsUpEnabled(false)
			it.setDisplayShowHomeEnabled(false)
		}

		initializeNotesRecyclerView()

		swipe_to_refresh.setOnRefreshListener {
			notesViewModel.refreshData()
		}

		add_note_button.setOnClickListener {
			navigationController?.goToDetail()
		}
	}

	/**
	 * Setup Notes recycler view, its adapter, and swipe to delete an item.
	 */
	private fun initializeNotesRecyclerView() {
		activity?.let {
			notesAdapter = NotesAdapter(this, it)

			notes_recycler_view.layoutManager = LinearLayoutManager(it)
			notes_recycler_view.adapter = notesAdapter

			// Setup swipe to delete.
			val itemTouchHelper = ItemTouchHelper(SwipeToDeleteCallback(notesAdapter!!))
			itemTouchHelper.attachToRecyclerView(notes_recycler_view)
		}
	}
}