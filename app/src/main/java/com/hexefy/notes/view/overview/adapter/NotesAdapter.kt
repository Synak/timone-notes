package com.hexefy.notes.view.overview.adapter

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.hexefy.notes.R
import com.hexefy.notes.model.database.entities.NoteEntity

/**
 * Created by Ondrej Synak on 3/22/19.
 *
 * Adapter for holding Note Entities and displaying them.
 */
class NotesAdapter(private val notesAdapterListener: NotesAdapterListener, val context: Context) :
		RecyclerView.Adapter<RecyclerView.ViewHolder>() {

	// Private Attributes **********************************************************************************************

	private var notes: MutableList<NoteEntity>? = null

	// Public Methods **************************************************************************************************

	override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder =
			NoteViewHolder(
					LayoutInflater.from(parent.context)
							.inflate(R.layout.overview_note_item, parent, false)
			)

	override fun getItemCount(): Int = notes?.size ?: 0

	override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
		notes?.get(position)?.let { note ->
			if (holder is NoteViewHolder) {
				holder.bind(note)
			}
		}
	}

	fun setData(items: List<NoteEntity>) {
		notes = items.toMutableList()
		notifyDataSetChanged()
	}

	fun deleteItem(position: Int) {
		notes?.get(position)?.let {
			notes?.remove(it)
			notifyItemRemoved(position)
			notesAdapterListener.onNoteDeleted(it)
		}
	}

	interface NotesAdapterListener {
		fun onNoteClicked(noteId: Int)
		fun onNoteDeleted(noteEntity: NoteEntity)
	}

	// View Holders ****************************************************************************************************

	inner class NoteViewHolder(itemView: View) : BaseViewHolder<NoteEntity>(itemView) {

		private val noteText: TextView = itemView.findViewById(R.id.note_text_view)
		var item: NoteEntity? = null
			private set

		override fun bind(item: NoteEntity) {
			this.item = item
			noteText.text = item.title
			itemView.setOnClickListener {
				notesAdapterListener.onNoteClicked(item.id!!)
			}
		}
	}

	/**
	 * Base Class for all view holders in NotesAdapter.
	 */
	abstract inner class BaseViewHolder<T>(itemView: View) : RecyclerView.ViewHolder(itemView) {

		/**
		 * Set layout according to passed data model.
		 */
		abstract fun bind(item: T)
	}
}