package com.hexefy.notes.view.base

import android.arch.lifecycle.ViewModelProvider
import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import android.support.v4.app.Fragment
import com.hexefy.notes.App
import com.hexefy.notes.view.base.navigation.NavigationController
import com.hexefy.notes.viewModel.NotesViewModel
import javax.inject.Inject


/**
 * Created by Ondrej Synak on 3/22/19.
 *
 * Abstract class for fragment operating with note/notes.
 * Class is responsible for loading view model and navigation controller.
 */
abstract class BaseNotesFragment : Fragment() {

	@Inject
	lateinit var modelFactory: ViewModelProvider.Factory

	// Protected Attributes ************************************************************************

	protected lateinit var notesViewModel: NotesViewModel

	protected val navigationController: NavigationController?
		get() {
			return if (activity != null) {
				(activity as NotesActivity).navigationController
			} else {
				null
			}
		}

	// Public Methods ******************************************************************************

	override fun onCreate(savedInstanceState: Bundle?) {
		super.onCreate(savedInstanceState)

		activity?.let {
			App.applicationComponent.inject(this)
			notesViewModel = ViewModelProviders.of(it, modelFactory).get(NotesViewModel::class.java)
		}
	}
}