package com.hexefy.notes.view.base.navigation

import android.support.v4.app.FragmentManager
import com.hexefy.notes.R
import com.hexefy.notes.view.detail.NoteDetailFragment
import com.hexefy.notes.view.overview.NotesOverviewFragment

/**
 * Created by Ondrej Synak on 3/22/19.
 *
 * Class for managing navigation between fragments.
 */
class NavigationController(
		private val supportFragmentManager: FragmentManager,
		private val fragmentContainerId: Int
) {

	fun goToOverView() {
		with(supportFragmentManager.beginTransaction()) {
			replace(fragmentContainerId, NotesOverviewFragment())
			commitAllowingStateLoss()
		}
	}

	fun goToDetail(noteId: Int? = null) {
		with(supportFragmentManager.beginTransaction()) {
			setCustomAnimations(
					R.anim.slide_in_right,
					R.anim.slide_out_left,
					R.anim.slide_in_left,
					R.anim.slide_out_right
			)
			addToBackStack(null)
			replace(fragmentContainerId, NoteDetailFragment.newInstance(noteId))
			commitAllowingStateLoss()
		}
	}
}