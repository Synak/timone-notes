package com.hexefy.notes

import android.app.Application
import android.content.Context
import com.hexefy.notes.dependencyInjection.components.ApplicationComponent
import com.hexefy.notes.dependencyInjection.components.DaggerApplicationComponent
import com.hexefy.notes.dependencyInjection.modules.AppModule
import com.hexefy.notes.dependencyInjection.modules.DatabaseModule
import com.hexefy.notes.dependencyInjection.modules.NetworkModule
import timber.log.Timber
import java.lang.ref.WeakReference

/**
 * Created by Ondrej Synak on 3/22/19.
 */
class App : Application() {

	companion object {
		lateinit var contextReference: WeakReference<Context>
		lateinit var applicationComponent: ApplicationComponent
	}

	// Public Methods ******************************************************************************

	override fun onCreate() {
		super.onCreate()

		contextReference = WeakReference(this)

		initializeTimberLogging()
		initializeApplicationComponent()
	}

	// Private Methods *****************************************************************************

	private fun initializeTimberLogging() {
		Timber.plant(Timber.DebugTree())
	}

	private fun initializeApplicationComponent() {
		applicationComponent = DaggerApplicationComponent.builder()
				.appModule(AppModule(this))
				.databaseModule(DatabaseModule())
				.networkModule(NetworkModule("https://private-anon-298be78c7d-note10.apiary-mock.com/"))
				.build()
	}
}