# Notes
## General

The goal of the application was to allows a user to view, create, edit and delete notes using given rest API even when device is offline. Everything is stored locally first and it is synched with server when it is possible. This gives user better usabillity for the application.

Unfortunately, the given restAPI is a dummy, so sometimes it creates weird situations. For example - user creates a note without internet connection, note is stored locally with given text but as soon as it is uploaded to the server the text is changed to something else.

## Used Patterns/Technologies

The app itself is written in Kotlin language. Using **MVVM** architecture from ([Android ModelViewViewModel](https://developer.android.com/jetpack/docs/guide)). 

* **Timber** - for enhanced logging.
* **Retrofit2** - network operations along with **RxJava2** which provides good control in repositories what should be loaded and from where.
* **Dagger2** - for dependency injection
* **Room** - for local storage and offline functionality
* **Mockito, JUnit** - for testing

## Desing/Functionality

The app consists of 2 screens.

First screen is overview of all notes user currently has.

* **Refresh data** is done when application is loaded or user swipes down in the list (Swipe to refresh)
* **Delete a note** is triggered by swipping note sideways
* **Edit a note** is possible by clicking on a note and going to second screen
* **Create a note** is triggered by floating action button

Second screen is for editing or creating new note.

* **Delete a note** is possible only in editing mode and it is done by clicking on a trash icon on action bar
* **Save a note/Create new one** is confirmed by floating action button.






